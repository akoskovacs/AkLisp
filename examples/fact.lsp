;; Write out the factorial of a given number using range
(display "n = ")
;;Multiply a range of numbers from 1 to the given number
(display "n! = " (* (range 1 (read-number))))
(newline)
